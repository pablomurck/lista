#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "lista.h"

// Cantidad de elementos que se insertaran a la lista en la prueba "masiva"
#define CANT_MASIVA 100000

/*******************************************************************
 *                        PRUEBAS UNITARIAS
 ******************************************************************/

/* Función auxiliar para imprimir si estuvo OK o no. */
void print_test(char* name, bool result)
{
	printf("%s: %s\n", name, result? "OK" : "ERROR");
}

/* Recibe una lista y corre los tests de la lista vacia */
void tests_lista_vacia(lista_t* lista)
{
	print_test("Lista esta Vacia es True", lista_esta_vacia(lista));
	print_test("Ver primero de la lista es NULL", lista_ver_primero(lista) == NULL);
	print_test("Borrar primero de una lista Vacia es NULL", lista_borrar_primero(lista) == NULL);
	print_test("El largo de la lista Vacia debe ser 0", lista_largo(lista) == 0);
}

/* Pruebas basicas para una lista vacia. */
void pruebas_lista_vacia()
{
	lista_t* lista = lista_crear();
	if (lista == NULL) return;
	printf("\n-PRUEBAS CON LISTA VACIA-\n");
	tests_lista_vacia(lista);
	lista_destruir(lista, NULL);
}

/* Pruebas de pocos datos y diferentes tipos. */
void pruebas_lista()
{
	lista_t* lista = lista_crear();
	if (lista == NULL) return;
	
	// Datos a insertar:
	char cadena[] = "Hola mundo";
	unsigned int dato_1 = 25000;
	int dato_2 = -1123;
	float dato_3 = 1.23456;
	short arreglo[] = {6,5,4,-3,2,5,-4,2,3,6,68};
	
	// Arreglo de punteros genericos para facilitar el proceso
	void* todos[] = {&dato_1, &dato_2, &dato_3, arreglo, cadena, NULL};
	size_t tam = 6;
	size_t i;
	
	printf("\n-PRUEBAS CON DIFERENTES TIPOS DE DATOS-\n");

	// Inserciones en primera posicion y comprobaciones
	for (i = 0; i < tam; i++)
	{
		printf("Trabajando con el dato %zu \n", i);
		print_test("Insertar el dato en PRIMER pos", lista_insertar_primero(lista, todos[i]));
		print_test("Ver el primero, debe ser el de recien", lista_ver_primero(lista) == todos[i]);
		print_test("La lista esta Vacia debe ser False", !lista_esta_vacia(lista));
		print_test("Verificar el largo", lista_largo(lista) == i+1);
	}

	// Borrar y comprobar
	while (i--)
	{
		printf("Trabajando con el dato %zu \n", i);
		print_test("Ver primero es el dato correspondiente", lista_ver_primero(lista) == todos[i]);
		print_test("La lista esta Vacia debe ser False", !lista_esta_vacia(lista));
		print_test("Borrar el primero y verificar el dato", lista_borrar_primero(lista) == todos[i]);
		print_test("Verificar el largo", lista_largo(lista) == i);
	}
	tests_lista_vacia(lista);

	// Inserciones en ultima posicion y comprobaciones
	for (i = 0; i < tam; i++)
	{
		printf("Trabajando con el dato %zu \n", i);
		print_test("Inserto el dato en ULTIMA pos", lista_insertar_ultimo(lista, todos[i]));
		print_test("Veo el primero, debe ser siempre el mismo", lista_ver_primero(lista) == todos[0]);
		print_test("La lista esta Vacia debe ser False", !lista_esta_vacia(lista));
		print_test("Verificar el largo", lista_largo(lista) == i+1);
	}

	// Borrar y comprobar
	for (i = 0; i < tam; i++)
	{
		printf("Trabajando con el dato %zu \n", i);
		print_test("Ver primero es el dato correspondiente", lista_ver_primero(lista) == todos[i]);
		print_test("La lista esta Vacia debe ser False", !lista_esta_vacia(lista));
		print_test("Borrar el primero y verificar el dato", lista_borrar_primero(lista) == todos[i]);
		print_test("Verificar el largo", lista_largo(lista) == (tam-i-1));
	}
	tests_lista_vacia(lista);

	// Insercion y borrado en ultimo y primero "secuencial"
	for (i = 0; i < tam; i++)
	{
		printf("Trabajando con el dato %zu \n", i);
		
		print_test("Inserto el dato en ULTIMA pos", lista_insertar_ultimo(lista, todos[i]));
		print_test("La lista esta Vacia debe ser False", !lista_esta_vacia(lista));
		print_test("El largo de la lista es 1", lista_largo(lista) == 1);	
		print_test("Ver primero es el dato correspondiente", lista_ver_primero(lista) == todos[i]);
		print_test("Borrar primero y verificar el dato", lista_borrar_primero(lista) == todos[i]);

		print_test("Inserto el dato en PRIMERA pos", lista_insertar_primero(lista, todos[i]));
		print_test("La lista esta Vacia debe ser False", !lista_esta_vacia(lista));
		print_test("El largo de la lista es 1", lista_largo(lista) == 1);
		print_test("Ver primero es el dato correspondiente", lista_ver_primero(lista) == todos[i]);
		print_test("Borrar primero y verificar el dato", lista_borrar_primero(lista) == todos[i]);
	}
	tests_lista_vacia(lista);
	lista_destruir(lista, NULL);
}

/* Pruebas de insercion de grandes cantidades de elementos. */
void pruebas_lista_masiva()
{
	lista_t* lista = lista_crear();
	if (lista == NULL) return;
	
	// Reservo memoria para mi arreglo de "CANT_MASIVA"
	size_t* arreglo = malloc(CANT_MASIVA * sizeof(size_t));
	if (arreglo == NULL)
	{
		lista_destruir(lista, NULL);
		return;
	}
	printf("\n-PRUEBAS CON GRANDES CANTIDADES DE ELEMENTOS-\n");
	printf("\nProbamos INSERCION masiva de %d elementos\n", CANT_MASIVA);

	size_t i;
	bool ok;
	for (ok = true, i = 0; i < CANT_MASIVA && ok; i++)
	{
		// Asigno valor para luego hacer una verificacion
		arreglo[i] = i;
		// Pruebas de insercion
		ok = ok && lista_insertar_ultimo(lista, (arreglo + i));
		ok = ok && lista_ver_primero(lista) == arreglo;
		// Verifico que no me haya cambiado los valores
		ok = ok && arreglo[i] == i;
	}
	print_test("Resultado total prueba INSERCION", ok);
	
	if (!ok) // Para especificar el error
		printf("Fallo en el elemento n %zu", i--);
	
	printf("\nProbamos BORRAR los elementos\n");
	for (ok = true, i = 0; i < CANT_MASIVA && ok; i++)
	{
		ok = ok && lista_ver_primero(lista) == (arreglo + i);
		ok = ok && lista_borrar_primero(lista) == (arreglo + i);
	}
	print_test("Resultado prueba BORRADO", ok);
	tests_lista_vacia(lista);
	// Libero el arreglo y destruyo la lista
	free(arreglo);
	lista_destruir(lista, NULL);
}

/* Recibe una lista (con elementos estaticos) que se destruira. */
void destruir_lista_estatica(void* lista)
{
	lista_destruir((lista_t*)lista, NULL);
}

/* Pruebas de destrucciones */
void pruebas_lista_destruir()
{
	lista_t* lista = lista_crear();
	if (lista == NULL) return;
	
	printf("\n-PRUEBAS DE DESTRUCCION-\n");
	printf("Pruebas de destruccion con elementos en memoria dinamica.\n");
	
	int* n = malloc(sizeof(int));
	if (n) lista_insertar_ultimo(lista, n);
	float* f = malloc(sizeof(float));
	if (f) lista_insertar_ultimo(lista, f);
	char* c = malloc(sizeof(char));
	if (c) lista_insertar_ultimo(lista, c);
	
	lista_destruir(lista, free);
	
	printf("Terminado, si pasa valgrind esta bien.\n");
	
	// Prueba de lista con listas de elementos estaticos
	lista_t* lista_principal = lista_crear();
	if (lista_principal == NULL) return;
	
	size_t tam = 10;
	int arreglo[tam];
	lista_t** punt_a_listas_estaticas = malloc(tam * sizeof(lista_t*));
	if (punt_a_listas_estaticas == NULL){
		lista_destruir(lista_principal, NULL);
		return;
	}
	
	printf("Pruebas de destruccion con listas con elementos estaticos.\n");
	
	for (size_t i=0; i<tam; i++){
		// Creo las listas "estaticas"
		punt_a_listas_estaticas[i] = lista_crear();
		if (punt_a_listas_estaticas[i] == NULL){
			printf("No se creo una de las listas dinamicas!!!!\n");
			continue;
		}
		// Inserto el elemento en la lista
		lista_insertar_ultimo(punt_a_listas_estaticas[i], (arreglo + i));
		// Inserto las listas estaticas en la lista principal
		lista_insertar_ultimo(lista_principal, punt_a_listas_estaticas[i]);
	}
	// Ahora hay una lista principal con "tam" listas con un elemento estatico cada una
	// Destruyo
	lista_destruir(lista_principal, destruir_lista_estatica);
	// limpio a los ayudantes
	free(punt_a_listas_estaticas);
	printf("Terminado, si pasa valgrind esta bien.\n");
}

/* Corre las pruebas del iterador en una lista vacia. */
void tests_iter_lista_vacia(lista_iter_t* iter)
{
	print_test("Iter en lista vacia: avanzar es False", !lista_iter_avanzar(iter));
	print_test("Iter en lista vacia: estar al final es True", lista_iter_al_final(iter));
	print_test("Iter en lista vacia: ver actual es NULL", !lista_iter_ver_actual(iter));
}

/* Pruebas del iter. externo con lista vacia y pocos elementos. */
void pruebas_iterador_externo_casos_borde()
{
	lista_t* lista = lista_crear();
	if (lista == NULL) return;

	lista_iter_t* iter = lista_iter_crear(lista);
	if (iter == NULL) 
	{
		lista_destruir(lista, NULL);
		return;
	}
	int primero = 420;
	int segundo = 666;

	printf("\n-PRUEBAS DEL ITERADOR EXTERNO: CASOS BORDE-\n");

	tests_iter_lista_vacia(iter);
	print_test("Inserto 1 elemento en lista vacia", lista_insertar(lista, iter, &primero));

	print_test("El largo es 1", lista_largo(lista) == 1);
	print_test("Ver primero es el de recien", lista_ver_primero(lista) == &primero);
	print_test("El actual del iter es el de recien", lista_iter_ver_actual(iter) == &primero);

	print_test("Avanzo para llegar al final", lista_iter_avanzar(iter));
	print_test("Estoy al final", lista_iter_al_final(iter));
	print_test("Inserto otro elemento al final", lista_insertar(lista, iter, &segundo));

	print_test("El largo es 2", lista_largo(lista) == 2);
	print_test("Ver primero sigue siendo el anterior", lista_ver_primero(lista) == &primero);
	print_test("El actual del iter es el de recien", lista_iter_ver_actual(iter) == &segundo);

	print_test("Borro el elemento y compruebo", lista_borrar(lista, iter) == &segundo);
	print_test("El largo es 1", lista_largo(lista) == 1);
	print_test("Ver primero sigue siendo el anterior", lista_ver_primero(lista) == &primero);

	print_test("El actual del iter es NULL", lista_iter_ver_actual(iter) == NULL);
	lista_iter_destruir(iter);

	printf("Destruyo y creo un nuevo iter\n");
	iter = lista_iter_crear(lista);
	if (iter == NULL)
	{
		lista_destruir(lista, NULL);
		return;
	}
	print_test("Borro el primero y compruebo", lista_borrar(lista, iter));
	tests_lista_vacia(lista);
	tests_iter_lista_vacia(iter);

	lista_iter_destruir(iter);
	lista_destruir(lista, NULL);
}

void pruebas_iterador_externo()
{
	lista_t* lista = lista_crear();
	if (lista == NULL) return;

	int arreglo[] = {1,2,3,4,5,6,7,8,9,10};
	size_t tam = 10;

	for (size_t i = 0; i < tam; i++)
		lista_insertar_ultimo(lista, arreglo + i);

	lista_iter_t* iter = lista_iter_crear(lista);
	if (iter == NULL)
	{
		lista_destruir(lista, NULL);
		return;
	}
	printf("\n-PRUEBAS DEL ITERADOR EXTERNO-\n");

	printf("\nPruebas de iteracion simple (sin mod)\n");
	for (size_t i = 0; i < tam; i++)
	{
		print_test("El iterador devuelve el elemento correcto", lista_iter_ver_actual(iter) == arreglo + i);
		print_test("NO estamos al final", !lista_iter_al_final(iter));
		print_test("Avanzamos es True", lista_iter_avanzar(iter));
	}
	print_test("SI estamos al final", lista_iter_al_final(iter));
	print_test("Avanzamos es False", !lista_iter_avanzar(iter));
	print_test("Ver actual es NULL", !lista_iter_ver_actual(iter));

	lista_iter_destruir(iter);
	iter = lista_iter_crear(lista);
	if (iter == NULL)
	{
		lista_destruir(lista, NULL);
		return;
	}

	// Queremos llegar a {1,1,2,2,3,3,...,10,10}
	// usando el iterador y sus inserciones
	printf("\nPruebas de iteracion con modificaciones\n");
	for (size_t i = 0; i < tam; i++)
	{
		printf("Dato %zu: ", (i+1));
		print_test("Insercion", lista_insertar(lista, iter, lista_iter_ver_actual(iter)));
		lista_iter_avanzar(iter);
		lista_iter_avanzar(iter);
	}
	print_test("Iter esta al final", lista_iter_al_final(iter));

	lista_iter_destruir(iter);
	iter = lista_iter_crear(lista);
	if (iter == NULL)
	{
		lista_destruir(lista, NULL);
		return;
	}

	for (size_t i = 0; i < tam; i++)
	{
		for (size_t j = 0; j < 2; j++)
		{
			printf("Equivalente a Dato %zu: ", (i+1));
			print_test("Borro el elemento",lista_borrar(lista, iter) == arreglo + i);
		}	
	}
	tests_lista_vacia(lista);
	tests_iter_lista_vacia(iter);

	lista_iter_destruir(iter);
	lista_destruir(lista, NULL);
}

/* Guarda la multiplacacion de n por multip en n. */
void multiplicar(int *n, int *multip)
{
	*n *= *multip;
}

bool wrapper_multiplicar(void *numero, void *multiplador)
{
	multiplicar((int*)numero, (int*)multiplador);
	return true;
}

void pruebas_iterador_interno_lista_vacia()
{
	lista_t* lista = lista_crear();
	if (lista == NULL) return;

	printf("\n-PRUEBAS DEL ITERADOR INTERNO EN LISTA VACIA-\n");
	int multip = 2;
	lista_iterar(lista, wrapper_multiplicar, &multip);
	tests_lista_vacia(lista);
	lista_destruir(lista, NULL);
}

void pruebas_iterador_interno()
{
	lista_t* lista = lista_crear();
	if (lista == NULL) return;

	int arreglo[] = {1,2,3,4,5,6,7,8,9,10};
	size_t tam = 10;
	for (size_t i = 0; i < tam; i++)
	{
		lista_insertar_ultimo(lista, arreglo + i);
	}
	printf("\n-PRUEBAS DEL ITERADOR INTERNO-\n");

	int multip = 2;
	lista_iterar(lista, wrapper_multiplicar, &multip);

	int arreglo_original[] = {1,2,3,4,5,6,7,8,9,10};

	for (size_t i = 0; i < tam; i++)
	{
		int* n = lista_borrar_primero(lista);
		printf("Dato %zu: ", (i+1));
		print_test("Iteracion y modifacion exitosa", *n == (arreglo_original[i] * multip));
	}
	tests_lista_vacia(lista);
	lista_destruir(lista, NULL);
}

/* Programa principal. */
int main(void)
{
	pruebas_lista_vacia();
	pruebas_lista();
	pruebas_lista_masiva();
	pruebas_lista_destruir();
	pruebas_iterador_externo_casos_borde();
	pruebas_iterador_externo();
	pruebas_iterador_interno_lista_vacia();
	pruebas_iterador_interno();
	return 0;
}
