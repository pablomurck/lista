# Makefile for executable adjust

# *****************************************************
# Parameters to control Makefile operation

CC = gcc
CFLAGS = -Wall -pedantic --std=c99 -g

VALGRIND= valgrind --track-origins=yes --show-reachable=yes --leak-check=full
VALGRIND-V= $(VALGRIND) -v

EXEC = prueba_lista

# ****************************************************
# Entries to bring the executable up to date

$(EXEC): lista.o prueba_lista.c
	$(CC) $(CFLAGS) -o $(EXEC) lista.o prueba_lista.c 

lista.o: lista.c lista.h
	$(CC) $(CFLAGS) -c lista.c

valgrind: $(EXEC)
	$(VALGRIND) ./$(EXEC)

valgrind-verb: $(EXEC)
	$(VALGRIND-V) ./$(EXEC)
